# Copyright (C) 2022 Kristian Blom and Aljaz Godec - All Rights Reserved  
# You may use, distribute and modify this code under the terms of the MIT license. 
# You should have received a copy of the MIT license with this file. 
# If not, please write to: kblom@mpinat.mpg.de

#Analytical packages
import scipy
import numpy as np
from scipy import special

#Log function
#----------------------------------------
def log(x):
    return np.log(x, dtype = 'f16')

#Exponential function
#----------------------------------------
def exp(x):
    return np.exp(x, dtype = 'f16')

#Tangent hyperbolicus
#----------------------------------------
def tanh(x):
    return np.tanh(x, dtype = 'f16')

#Gamma function
#----------------------------------------
def gamma_tilde(x,z):
    return scipy.special.gammaln(z*x/2+1)

#Force term Hamiltonian
#----------------------------------------
def H_f_vec(f, phi, alpha):
    return(-2*f*(1/(phi+alpha)-1))

def dH_f_vec(f, phi, alpha):
    return(2*f/(phi+alpha)**2)

def ddH_f_vec(f, phi, alpha):
    return(-4*f/(phi+alpha)**3)

#Intrinsic binding affinity term Hamiltonian
#----------------------------------------
def H_mu_vec(mu, phi, num_spins):
    return(-mu*phi*num_spins)

def dH_mu_vec(mu, phi, num_spins):
    return(-mu*num_spins)
        
#Finite systems
#---------------------------------------------------------------------
#---------------------------------------------------------------------
#---------------------------------------------------------------------

#Degeneracy factor
#--------------------------------------
def h_func(z, N, Nc, Noc):
    
    I   = gamma_tilde(N,z)
    II  = gamma_tilde(Nc-Noc,z)
    III = gamma_tilde(N-Nc-Noc,z)
    IV  = gamma_tilde(Noc,z)                          
    
    return(I-II-III-2*IV)

#Xbar
#--------------------------------------
def Xbar_func(Xstar, J, num_spins):
    
    eta   = np.exp(2*J)
    I     = 2*Xstar
    II    = 1+(1+4*(Xstar/num_spins)*(eta**2-1))**(1/2)
    
    return(I/II)
        
#Free-energy density
#--------------------------------------
def f_finite(num_spins, f, mu, alpha, J, z, MF):

    #Initialize num_spins array
    num_spins_arr = np.arange(0, num_spins+1, 1)
    
    #Apply meshgrid
    num_spins_arr, f, mu, J = np.meshgrid(num_spins_arr, f, mu, J, indexing='ij')
    
    #Set constants
    Nc, Nu = num_spins_arr, num_spins-num_spins_arr
    phi    = Nc/num_spins
    
    #Xstar and Xbar
    Xstar       = phi*Nu
    if MF: Xbar = Xstar
    else:  Xbar = Xbar_func(Xstar, J, num_spins)
            
    #Entropy
    A          = scipy.special.gammaln(num_spins+1)-scipy.special.gammaln(Nc+1)-scipy.special.gammaln(Nu+1)
    hup, hdown = h_func(z, num_spins, Nc, Xbar), h_func(z, num_spins, Nc, Xstar)
                   
    #Energy densities
    Ej = -2*z*J*(Xbar/num_spins-(1/4))
    Ef = H_f_vec(f, phi, alpha)/num_spins
    Em = H_mu_vec(mu, phi, num_spins)/num_spins
        
    #Free energy density
    farr  = (-A-hup+hdown)/num_spins-Ej+Ef+Em
    
    #Delete independent variables from memory
    del num_spins_arr, f, mu, J
    
    return(farr)

#Equation of state
#--------------------------------------
def EoS_finite(num_spins, f, mu, alpha, J, z, MF):
    
    #Calculate free-energy density
    f_arr = f_finite(num_spins, f, mu, alpha, J, z, MF)
        
    #Calculate Equation of State
    temp  = np.arange(0, num_spins+1, 1)/num_spins
    temp2 = exp(-num_spins*f_arr)
    Eos   = np.sum(temp[:,np.newaxis,np.newaxis,np.newaxis]*temp2, axis=0)/np.sum(temp2, axis=0)
    
    return(Eos)
