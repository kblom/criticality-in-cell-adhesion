# Copyright (C) 2022 Kristian Blom and Aljaz Godec - All Rights Reserved  
# You may use, distribute and modify this code under the terms of the MIT license. 
# You should have received a copy of the MIT license with this file. 
# If not, please write to: kblom@mpinat.mpg.de

#Analytical packages
import scipy
import warnings
import numpy as np
from scipy.sparse import linalg, csr_matrix, SparseEfficiencyWarning

#External package
from managing_tools import *

#Surpress sparse efficiency warning
warnings.simplefilter('ignore',SparseEfficiencyWarning)


#Full matrix symmetrizer (Here we use the detailed-balance relation!)
#--------------------------------------
def matrix_symmetrizer(temp1, temp2, data_arr, row_arr, col_arr, num_spins):
    
    #Construct the transition matrix
    Tfull = csr_matrix((data_arr, (row_arr, col_arr)), shape=(2**num_spins, 2**num_spins))

    #Add lower diagonal part
    Tfull += np.transpose(scipy.sparse.diags(temp1**2,0)*Tfull*scipy.sparse.diags(temp2**2,0)) 
    
    #Add diagonal
    Tfull.setdiag(-Tfull.sum(axis=0).A1)
        
    #Symmetrize the transition matrix
    Tfull = scipy.sparse.diags(temp1,0)*Tfull*scipy.sparse.diags(temp2,0)
       
    return(Tfull)

#Local equilibrium MFPT calculator
#--------------------------------------
def leq_MFPT_calculator(T, Zpar):
    
    #Set constants 
    A, B = np.cumsum(Zpar, axis = 0), T*Zpar
    
    #Calculate mean first passage times (Based on Eq.22 in the paper)
    leq_MFPT_d = np.sum((A[-1,:]-A[0:-1,:])/B[1::,:],  axis=0)
    leq_MFPT_n = np.sum(A[0:-1,:]/B[1::,:],            axis=0)
    
    return(leq_MFPT_d, leq_MFPT_n)
        
#Exact MFPT calculator
#--------------------------------------
def full_MFPT_calculator(Jlen, num_spins, Pb, data_arr, row_arr, col_arr, atol):
    
    #Initialize data arrays
    full_MFPT_d, full_MFPT_n = np.zeros((Jlen), dtype='f8'), np.zeros((Jlen),     dtype='f8')
    bd, bn = np.zeros(2**num_spins - 1, dtype = 'f8'), np.zeros(2**num_spins - 1, dtype = 'f8')
    
    #Set pulling/pushing settings
    begin_d, end_d, start_d = 0, 2**num_spins-1, 0
    begin_n, end_n, start_n = 1, 2**num_spins, -1

    #Calculate first and second moment 
    for i in range(Jlen):
        
        #Set constants
        temp1, temp2 = Pb[:,i]**(-1/2), Pb[:,i]**(+1/2)
        
        #Construct transition matrix
        Tfull = matrix_symmetrizer(temp1, temp2, data_arr[:,i], row_arr, col_arr, num_spins)

        #Output array: we solve the equation Tfull*x=b
        bd[start_d] = temp1[start_d]
        bn[start_n] = temp1[start_n]
    
        #Solve the matrix equation Ax = b by the conjugate gradient method
        x_dissolution = scipy.sparse.linalg.cg(Tfull[begin_d:end_d, begin_d:end_d], bd, tol = atol)[0]
        x_nucleation  = scipy.sparse.linalg.cg(Tfull[begin_n:end_n, begin_n:end_n], bn, tol = atol)[0]     
            
        #Rescale x1 with the Boltzmann weight
        x_dissolution *= temp2[begin_d:end_d]
        x_nucleation  *= temp2[begin_n:end_n]
        
        #First moment
        full_MFPT_d[i], full_MFPT_n[i] = np.sum(x_dissolution), np.sum(x_nucleation)
        
    return(full_MFPT_d, full_MFPT_n)

#Mean first passage time manager
#--------------------------------------
def MFPT_calculator_manager(Jlen, f, mu, num_spins, full_matrix, atol, dim3):
    
    #Set settings string
    settings_string, output_folder  = filename_maker(f, mu, -1)
    
    #Open output files
    T, Zpar = leq_file_maker(output_folder, settings_string, num_spins+1, Jlen, 'r')
    
    #Calculate leq MFPT to cluster dissolution / nucleation starting with N_closed_bonds0 closed bonds
    leq_MFPT_d, leq_MFPT_n = leq_MFPT_calculator(T, Zpar)
    
    #The same for the exact transition matrix
    if full_matrix == True:
        dim1, dim2                     = 2**num_spins, Jlen
        Pb, data_arr, row_arr, col_arr = full_file_maker(output_folder, settings_string, dim1, dim2, dim3, 'r')
        
        full_MFPT_d, full_MFPT_n = full_MFPT_calculator(Jlen, num_spins, Pb, data_arr, row_arr, col_arr, atol)
    else: 
        full_MFPT_d, full_MFPT_n = np.zeros((Jlen), dtype='f8'), np.zeros((Jlen), dtype='f8')
    
    return(full_MFPT_d, full_MFPT_n, leq_MFPT_d, leq_MFPT_n)

#Equation of state manager
#--------------------------------------
def Eos_calculator_manager(Jlen, f, mu, num_spins):
    
    #Set settings string
    settings_string, output_folder  = filename_maker(f, mu, -1)
    
    #Open output files
    T, Zpar = leq_file_maker(output_folder, settings_string, num_spins+1, Jlen, 'r')
    
    #Calculate the equation of state
    phi = np.array([np.sum(np.arange(0, num_spins+1,1)*Zpar[:,i])/np.sum(Zpar[:,i]) for i in range(0, Jlen)])/num_spins
    
    return(phi)
