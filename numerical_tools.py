# Copyright (C) 2022 Kristian Blom and Aljaz Godec - All Rights Reserved  
# You may use, distribute and modify this code under the terms of the MIT license. 
# You should have received a copy of the MIT license with this file. 
# If not, please write to: kblom@mpinat.mpg.de

#Analytical packages
import numpy as np

#Numba package
import numba as nb
from numba import njit, b1, i1, i4, i8, f8

#Coupling term Hamiltonian
#----------------------------------------
@njit('f8[:,:](f8[:], i8[:])')
def H_J(J, n_n_sum):
    return(-np.outer(J,n_n_sum))

#Uniform force term Hamiltonian
#----------------------------------------
#The alpha here is normally set to 1. However, one can choose it differently to play with the force load. 
@njit('f8[:](f8, f8[:], f8)')
def H_f(f, phi, alpha):
    return(-2*f*(1/(phi+alpha)-1))

#Intrinsic binding affinity term Hamiltonian
#----------------------------------------
@njit('f8[:](f8, f8[:], i1)')
def H_mu(mu, phi, num_spins):
    return(-mu*phi*num_spins)

#Boltzmann weight table
#----------------------------------------
@njit('Tuple((f8[:,:], f8[:], f8[:]))(f8[:], i8, f8, f8, f8, i1)')
def Boltzmann_weight_table(J, max_total_aligned, f, mu, alpha, num_spins):
        
    #Construct Boltzmann array for coupling strength
    #-----------------------------------------------
    
    #Set variables
    n_n_sum = max_total_aligned - np.arange(0, 2*max_total_aligned+1, 1)
        
    #Hamiltonian
    HJ= H_J(J, n_n_sum)
        
    #Boltzmann weight
    Boltz_J = np.exp(-HJ)
        
    #Construct Boltzmann array for external force and binding affinity
    #-----------------------------------------------------------------
    
    #Set variables
    phi = np.arange(0,num_spins+1,1)/num_spins
                
    #Hamiltonian
    Hf, Hmu = H_f(f, phi, alpha), H_mu(mu, phi, num_spins)
                
    #Boltzmann weight
    Boltz_f, Boltz_mu = np.exp(-Hf), np.exp(-Hmu)
            
    return(Boltz_J, Boltz_f, Boltz_mu)

#Transition rates
#----------------------------------------
@njit('f8[:,:](f8[:],f8[:,:],f8)')
def Glauber_transition_rate(Boltz1,Boltz2,p):
    return(p/(1+Boltz1/np.transpose(Boltz2)))

@njit('f8[:](f8[:],f8[:],f8)')
def Kawasaki_transition_rate(Boltz1,Boltz2,q):
    return(q/(1+Boltz1/Boltz2))

#Constant and data array initializer
#----------------------------------------
@njit('Tuple((i8[:], i8[:], i8, i8, i8))(f8[:,:], i8[:,:])')
def constant_initializer(Boltz_J,interaction_matrix):
    
    #Set constants
    Jlen                   = np.array(Boltz_J.shape)[0]
    num_spins              = len(interaction_matrix)
    sum_interaction_matrix = interaction_matrix.sum(axis=0)
    row_arr_table          = 2**(num_spins-1-np.arange(0,num_spins,1))
    
    return(sum_interaction_matrix, row_arr_table, Jlen, num_spins, 0)

@njit('Tuple((f8[:,:], f8[:,:]))(i8, i8)')
def data_initializer_leq(num_spins, Jlen):

    A = np.zeros((num_spins+1, Jlen), dtype = f8)
    B = np.zeros((num_spins+1, Jlen), dtype = f8)
    
    return(A,B)

@njit('Tuple((f8[:,:], f8[:,:], i4[:], i4[:]))(i8, i8, i8, i8, b1)')
def data_initializer_full(begin, end, num_spins, Jlen, kawasaki):
    
    #Set dimensions
    dim1 = end-begin
    if kawasaki: dim2 = num_spins*(1+num_spins)
    else:        dim2 = num_spins

    A  = np.empty((dim1, Jlen),      dtype = f8)
    B  = np.empty((dim1*dim2, Jlen), dtype = f8)
    C  = np.empty(dim1*dim2,         dtype = i4)
    D  = np.empty(dim1*dim2,         dtype = i4)
    
    return(A,B,C,D)

#Binary representation
#----------------------------------------
@njit('Tuple((i1[:], i1))(i8, i1)')
def binary_converter(index, num_spins):
    
    #Initialize arrays
    down_loc = np.zeros(num_spins, dtype = i1)
    
    #Initialize number of open bonds
    number_of_closed_bonds = 0
    
    #Create state array
    for i in range(num_spins):
        
        #Create spin-bit
        bit = index%2
                
        if bit == 0:
            
            #Update position of up bonds
            down_loc[number_of_closed_bonds] = num_spins - 1 - i
            
            #Update number of open bonds
            number_of_closed_bonds += 1
        
        #Update index
        index = index//2
        
    return(down_loc[0:number_of_closed_bonds], number_of_closed_bonds)

#Non-uniform force correction factors
#----------------------------------------
@njit('Tuple((f8,f8[:]))(f8[:], i1[:], i1, i1, b1)')
def non_uniform_force_correction(latt_dist, down_loc, Nc, Nc2, non_uniform_force):
    
    if non_uniform_force == True and Nc > 1:
        temp  = latt_dist[down_loc]
        corr1 = np.sum(temp)
        corr2 = (corr1-temp)/Nc2
        corr1 /= Nc
    elif Nc == 1:
        corr1 = np.sum(latt_dist[down_loc])
        corr2 = np.ones((Nc), dtype = f8)
    else:
        corr1 = 1
        corr2 = np.ones((Nc), dtype = f8)
    
    return(corr1, corr2)

#Neighbour statistics
#----------------------------------------
@njit('Tuple((i8[:],i1))(i8[:,:], i8[:], i1[:])')
def neighbour_statistics(interaction_matrix, sum_interaction_matrix, down_loc):
    
    #Reminder:
    #The interaction energy can be written as: (2*s-1)*J*(2*s-1)/2= 2*s*J*s-2*s*J*1+1*J*1/2
    #where s is a binary vector with 0: spin down and 1: spin up, and the factor /2 is needed for 
    #the overcounting in the interaction matrix (which been accounted for by dividing the interaction
    #matrix by a factor 2). The last term is a constant term independent of s, which has been accounted 
    #for in the Boltzmann array. 
    
    #Neighbour vector/ Neighbour scalar
    A  = interaction_matrix[:,down_loc].sum(axis = 1)
    B  = A[down_loc]
    C  = B.sum()
    D  = A.sum()

    return(2*B-sum_interaction_matrix[down_loc],D-C)

#Local equilibrium transition matrix filler
#--------------------------------------------
@njit('f8[:,:](f8[:,:], f8[:,:], f8[:], i1)')
def leq_transition_matrix_filler(T, Glauber_backward_rate, Boltz, number_of_closed_bonds):
           
    #Local equilibrium rate
    backward_transition  = np.sum(Glauber_backward_rate, axis=0)
    
    #Set element in local equilibrium transition matrix
    T[number_of_closed_bonds,:] += Boltz*backward_transition
                    
    return(T)  

#Exact transition matrix filler
#--------------------------------------------

#Glauber dynamics
@njit('Tuple((f8[:,:], i4[:], i4[:], i4))(f8[:,:], f8[:,:], i8[:], i4[:], i4[:], i1[:], i8, i4, i1, i1)')
def glauber_transition_matrix_filler(data_arr, Glauber_backward_rate, row_arr_table, row_arr, col_arr, down_loc, i, count, num_spins, number_of_closed_bonds):
                       
    #Exact transition matrix
    col_arr[count:count+number_of_closed_bonds]    = i
    row_arr[count:count+number_of_closed_bonds]    = i + row_arr_table[down_loc]
    data_arr[count:count+number_of_closed_bonds,:] = -Glauber_backward_rate
    
    #Update counter
    count += number_of_closed_bonds
                               
    return(data_arr, row_arr, col_arr, count)  

#Kawasaki dynamics
@njit('Tuple((f8[:,:], i4[:], i4[:], i4))(f8[:,:], f8[:], i4[:], i4[:], i8, i8, i4)')
def kawasaki_transition_matrix_filler(data_arr, Kawasaki_rate, row_arr, col_arr, row_arr_ind, i, count):
                       
    #Exact transition matrix
    col_arr[count]    = i
    row_arr[count]    = i + row_arr_ind
    data_arr[count,:] = -Kawasaki_rate
    
    #Update counter
    count += 1
                               
    return(data_arr, row_arr, col_arr, count)  

#Matrix filler
#----------------------------------------
@njit('Tuple((f8[:,:], f8[:,:], f8[:,:], f8[:,:], i4[:], i4[:]))(f8[:,:], f8[:], f8[:], i8[:,:], f8[:], i8, i8, f8, b1, b1, b1)')
def brute_force_matrix_filler(Boltz_J, Boltz_f, Boltz_mu, interaction_matrix, latt_dist, begin, end, p, full_matrix, kawasaki, non_uniform_force):
    
    #Set constants
    sum_interaction_matrix, row_arr_table, Jlen, num_spins, count = constant_initializer(Boltz_J, interaction_matrix)
    
    #Initialize data arrays (local equilibrium assumption)
    T, Zpar = data_initializer_leq(num_spins, Jlen)
    
    #Initialize data arrays (for full transition matrix)
    if full_matrix: Pb, data_arr, row_arr, col_arr = data_initializer_full(begin, end, num_spins, Jlen, kawasaki)
    else:           Pb, data_arr, row_arr, col_arr = data_initializer_full(0, 0, 0, 0, False)
        
    #Set interaction list for kawasaki dynamics
    if kawasaki: 
        interaction_list, interaction_matrix2 = [], np.triu(interaction_matrix)
        for i in range(num_spins): interaction_list.append(np.where(interaction_matrix2[i,:] != 0)[0])
    
    #Loop over all the elements in the transition matrix
    for i in range(begin, end):
        
        #Initial state in binary number
        down_loc, Nc = binary_converter(i, num_spins) 
        
        #Set number of closed bonds in next step
        Nc2 = Nc-1
        
        #Neighbour vector/ Neighbour scalar
        s_neighbours_vec, s_neighbours_scal = neighbour_statistics(interaction_matrix, sum_interaction_matrix, down_loc)
       
        #Correction factors for non-uniform force
        corr1, corr2 = non_uniform_force_correction(latt_dist, down_loc, Nc, Nc2, non_uniform_force)
            
        #Boltzmann weight of initial and final configuration 
        Boltz1  = Boltz_J[:,s_neighbours_scal]*Boltz_mu[Nc]*(Boltz_f[Nc]**corr1)
        Boltz2  = Boltz_J[:,s_neighbours_scal+s_neighbours_vec]*Boltz_mu[Nc2]*(Boltz_f[Nc2]**corr2)
                   
        #Update the fixed magnetization partition function
        Zpar[Nc,:] += Boltz1
        
        #Transition rate
        Glauber_backward_rate = Glauber_transition_rate(Boltz1,Boltz2,p)
        
        #Fill local equilibrium transition matrix
        T = leq_transition_matrix_filler(T, Glauber_backward_rate, Boltz1, Nc)
         
        #Fill the full transition matrix
        if full_matrix:
            data_arr, row_arr, col_arr, count = glauber_transition_matrix_filler(data_arr, Glauber_backward_rate,
                                                                              row_arr_table, row_arr,
                                                                              col_arr, down_loc, i, count, 
                                                                              num_spins, Nc)
            Pb[i-begin,:] = Boltz1
            
            #Kawasaki dynamics for full transition matrix
            if kawasaki:
                #Loop over all down spins
                for ind, j in enumerate(down_loc):
                    down_loc2 = np.copy(down_loc)
                    #Loop over all nearest neighbor spins
                    for k in interaction_list[j]:
                        #Check if the nearest neighbor spin is not down
                        if np.prod(down_loc-k) != 0:
                            #Interchange the spins
                            down_loc2[ind]    = k
                            #Set the row number for the transition in the transition matrix
                            row_arr_ind       = row_arr_table[j]-row_arr_table[k]
                            #Determine energy of new state
                            s_neighbours_scal = neighbour_statistics(interaction_matrix, sum_interaction_matrix, down_loc2)[1]
                            #Correction factors for non-uniform force
                            corr3             = non_uniform_force_correction(latt_dist, down_loc2, Nc, Nc2, non_uniform_force)[0]
                            #Boltzmann weight
                            Boltz3            = Boltz_J[:,s_neighbours_scal]*Boltz_mu[Nc]*(Boltz_f[Nc]**corr3)
                            #Kawasaki rate
                            Kawasaki_rate     = Kawasaki_transition_rate(Boltz1,Boltz3,1-p)
                            #Fill transition matrix
                            data_arr, row_arr, col_arr, count  = kawasaki_transition_matrix_filler(data_arr, Kawasaki_rate, row_arr, col_arr, row_arr_ind, i, count)
                
                                              
    return(T, Pb, Zpar, data_arr[0:count], row_arr[0:count], col_arr[0:count])
        
