# Copyright (C) 2022 Kristian Blom and Aljaz Godec - All Rights Reserved  
# You may use, distribute and modify this code under the terms of the MIT license. 
# You should have received a copy of the MIT license with this file. 
# If not, please write to: kblom@mpinat.mpg.de

#Computer management packages
import os

#Analytical packages
import numpy as np
import networkx as nx


#External package
from numerical_tools import *

#Filename maker
#--------------------------------------
def filename_maker(f, mu, jobnum):
    
    #Set settings string
    settings_string = 'f_' + str(f) + '_mu_' + str(mu) + '_job_' + str(jobnum)
       
    #Set folder name output file
    #Change this output folder to folder where 
    #you want to save the transition matrix and Boltzmann array!
    output_folder = '.'
    
    return(settings_string, output_folder)

#File makers
#--------------------------------------
def leq_file_maker(output_folder, settings_string, dim1, dim2, _mode):
    
    #Set output file names
    output_filename_T               = os.path.join(output_folder, 'T_'    + settings_string)
    output_filename_Zpar            = os.path.join(output_folder, 'Zpar_' + settings_string)

    #Create output files
    output_file_T    = np.memmap(output_filename_T,    dtype = 'f8', shape=(dim1, dim2), mode = _mode)
    output_file_Zpar = np.memmap(output_filename_Zpar, dtype = 'f8', shape=(dim1, dim2), mode = _mode)
    
    return(output_file_T, output_file_Zpar)

def full_file_maker(output_folder, settings_string, dim1, dim2, dim3, _mode):
    
    #Set output file names
    output_filename_Pb       = os.path.join(output_folder, 'Pb_'       + settings_string)
    output_filename_data_arr = os.path.join(output_folder, 'data_arr_' + settings_string)
    output_filename_row_arr  = os.path.join(output_folder, 'row_arr_'  + settings_string)
    output_filename_col_arr  = os.path.join(output_folder, 'col_arr_'  + settings_string)
        
    #Create output files
    output_file_Pb       = np.memmap(output_filename_Pb,       dtype = 'f8', shape=(dim1, dim2), mode = _mode)
    output_file_data_arr = np.memmap(output_filename_data_arr, dtype = 'f8', shape=(dim3, dim2), mode = _mode)
    output_file_row_arr  = np.memmap(output_filename_row_arr,  dtype = 'i4', shape=(dim3),       mode = _mode)
    output_file_col_arr  = np.memmap(output_filename_col_arr,  dtype = 'i4', shape=(dim3),       mode = _mode)
    
    return(output_file_Pb, output_file_data_arr, output_file_row_arr, output_file_col_arr)

#Matrix construction manager
#--------------------------------------
def matrix_construct(J, f, mu, alpha, p, interaction_matrix, latt_dist, intervals, jobnum, full_matrix, kawasaki, non_uniform_force):      
           
    #Set constants
    Jlen, num_spins, max_total_aligned = len(J), len(interaction_matrix), np.int(np.sum(interaction_matrix)/2)
     
    #Boltzmann weight array
    Boltz_J, Boltz_f, Boltz_mu = Boltzmann_weight_table(J, max_total_aligned, f, mu, alpha, num_spins)
    
    #Set begin and end of summation
    begin, end = jobnum*intervals, (jobnum+1)*intervals
    
    #---------------------------------------------------------------
        
    #Run Forest run!
    T, Pb, Zpar, data_arr, row_arr, col_arr = brute_force_matrix_filler(Boltz_J, Boltz_f, Boltz_mu, 2*interaction_matrix, latt_dist, begin, end, p, full_matrix, kawasaki, non_uniform_force)
    
    #Local equiliibrium matrix
    #---------------------------------------------------------------
    
    #Define output folder and name for files
    settings_string, output_folder = filename_maker(f, mu, jobnum)
    
    #Local equilibrium files
    output_file_T, output_file_Zpar = leq_file_maker(output_folder, settings_string, num_spins+1, Jlen, 'w+')
    
    #Dump data and then delete from memory
    output_file_T[:,:], output_file_Zpar[:,:] = T/num_spins, Zpar
    del T, Zpar, output_file_T, output_file_Zpar
   
    #Full matrix
    #---------------------------------------------------------------
    dim3 = 0
    if full_matrix == True: 
        dim1, dim2, dim3 = intervals, Jlen, len(row_arr)
        output_file_Pb, output_file_data_arr, output_file_row_arr, output_file_col_arr = full_file_maker(output_folder, 
                                                                                                         settings_string, 
                                                                                                         dim1, dim2, dim3, 
                                                                                                         'w+')
        output_file_Pb[:,:]        = Pb
        output_file_data_arr[:,:]  = data_arr/num_spins
        output_file_row_arr[:]     = row_arr
        output_file_col_arr[:]     = col_arr
        del Pb, data_arr, row_arr, col_arr, output_file_Pb, output_file_data_arr, output_file_row_arr,  output_file_col_arr
        
    return(dim3)

#Matrix merger manager
#--------------------------------------
def matrix_merger(Jlen, f, mu, num_spins, numjobs, full_matrix, dim_arr):  

    #Set constants
    dim1      = 2**num_spins
    dim3      = np.concatenate([[0],np.cumsum(dim_arr)])
    intervals = int(dim1/numjobs)
    
    #Set settings string
    settings_string, output_folder = filename_maker(f, mu, -1)
              
    #Create output files
    f_output_file_T, f_output_file_Zpar = leq_file_maker(output_folder, settings_string, num_spins+1, Jlen, 'w+')
   
    if full_matrix == True: 
        f_output_file_Pb, f_output_file_data_arr, f_output_file_row_arr, f_output_file_col_arr = full_file_maker(output_folder, settings_string, dim1, Jlen, dim3[-1], 'w+')
        ind_old, ind_new = 0, 0

    #Construct the local equilibrium transition_matrix and partition function
    for i in range(numjobs):
        
        #Set settings string
        settings_string, output_folder = filename_maker(f, mu, i)
         
        #Open output files
        output_file_T, output_file_Zpar = leq_file_maker(output_folder, settings_string, num_spins+1, Jlen, 'r')
            
        #Update transition matrix and partition function
        f_output_file_T[:,:]    += output_file_T[:,:]
        f_output_file_Zpar[:,:] += output_file_Zpar[:,:]
        
        #Delete old files
        os.remove(output_file_T.filename), os.remove(output_file_Zpar.filename)
            
        if full_matrix == True:
            
            #Open output files
            output_file_Pb, output_file_data_arr, output_file_row_arr, output_file_col_arr = full_file_maker(output_folder, 
                                                                                                             settings_string,
                                                                                                             intervals, Jlen,
                                                                                                             dim_arr[i], 'r')
            #Update Boltzmann array
            f_output_file_Pb[i*intervals:(i+1)*intervals,:] = output_file_Pb[:,:]
            
            #Update transition matrix and partition function
            f_output_file_data_arr[dim3[i]:dim3[i+1],:] = output_file_data_arr[:,:]
            f_output_file_row_arr[dim3[i]:dim3[i+1]]    = output_file_row_arr[:]
            f_output_file_col_arr[dim3[i]:dim3[i+1]]    = output_file_col_arr[:]
            
            #Delete old files
            os.remove(output_file_Pb.filename),      os.remove(output_file_data_arr.filename)
            os.remove(output_file_row_arr.filename), os.remove(output_file_col_arr.filename)
            
            
    #Normalize the local equilibrium transition rates
    f_output_file_T[:,:] /= f_output_file_Zpar[:,:]
                   
    #Delete memory from RAM
    del f_output_file_T, f_output_file_Zpar
    if full_matrix == True: 
        del f_output_file_Pb, f_output_file_data_arr, f_output_file_row_arr, f_output_file_col_arr
        
    return(dim3[-1])
        
#Lattice maker
#--------------------------------------
def lattice_maker(lattice_name, Nx, Ny, periodic_binary, alpha):

    #Set topology dictionary
    Topology_list = {
    'Rectangular':      nx.generators.lattice.grid_2d_graph(Nx, Ny,            periodic = periodic_binary),
    'Triangular':       nx.generators.lattice.triangular_lattice_graph(Nx, Ny, periodic = False), 
    'Honeycomb':        nx.generators.lattice.hexagonal_lattice_graph(Nx,  Ny, periodic = False),
    'Circular':         nx.generators.classic.circular_ladder_graph(int(Nx*Ny/2)), 
    'Wheel':            nx.generators.classic.wheel_graph(Nx*Ny), 
    #'Desargues':        nx.generators.small.desargues_graph(), 
    #'Moebius kantor':   nx.generators.small.moebius_kantor_graph(), 
    #'Random':           nx.generators.geometric.random_geometric_graph(Nx*Ny, radius = 0.2, dim = 2), 
    #'Random tree':      nx.generators.trees.random_tree(Nx*Ny),
    #'Growing network' : nx.generators.directed.gn_graph(Nx*Ny), 
    #'Lobster':          nx.generators.random_graphs.random_lobster(Nx*Ny, p1 = 0.21, p2 = 0.5)
    }

    #Set lattice topology
    G = Topology_list[lattice_name]

    #Set interaction matrix
    interaction_matrix = nx.adjacency_matrix(G).todense().astype('i8')
    
    #Set number of bonds
    num_bonds = len(interaction_matrix)
    
    #Set average coordination number
    z = interaction_matrix.sum()/num_bonds
    
    #Set lattice distance measure
    latt_dist  = (np.array(list(nx.eccentricity(G).items()))[:,1]-nx.radius(G, e=None, usebounds=False)).astype('f2')
    latt_dist /= np.max(latt_dist)
    latt_dist  = 1/np.sqrt((alpha-latt_dist**2))
    
    #Normalize lattice distance with the external force
    latt_dist *= num_bonds/(np.sum(latt_dist))

    return(interaction_matrix, num_bonds, z, latt_dist)
        
