This source code has been used in the following paper: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.11.031067

Please note that for the attached source code the following version of Python was used: 3.7.3 

The following Python packages are required to run the source code: joblib, matplotlib, numpy, numba, networkx, and scipy.

Summary of the main files: 																																									.
1. Main_notebook.ipynb contains the main Python Jupyter notebook from which all calculations are executed and plotted.

2. managing_tools.py contains the functions to save (in parallel) the exact full and local equilibrium (leq) transition matrix + Boltzmann array.

3. numerical_tools.py contains the functions to construct the exact full and leq transition matrix + Boltzmann array.

4. calculation_tools.py contains the functions to calculate the exact mean first passage times (based on numerical inversion) + exact equation of state for finite-size systems.

5. approximate_tools_equilibrium.py contains the functions to calculate the equation of state based on the mean field (MF) and Bethe-Guggenheim (BG) approximation.

Please follow these (optional) steps before executing the code in Main_notebook.ipynb:

1. Open managing_tools.py with a text editor.

2. Go to the first function: filename_maker(f, mu, jobnum)

3. Change the variable output_folder to the output folder name where you want to save the transition matrix and Boltzmann array.

4. Save managing_tools.py.

5. Now you can execute the code in Main_notebook.ipynb.

Summary of running times + memory load 

Computer specifics: Intel(R) Xeon(R) E5-1630 v4 3.70 GHz with 8 cores. 

Model specifics: (Nx,Ny)=(3,7), (h, mu, J) = (0, 0.2, np.arange(0, 1.0+0.05, 0.05)), rel_err == 10**-5, full_matrix == True, non_uniform_force == True, Kawasaki == True.

1. Constructing all Boltzmann weights and full + leq transition matrix) Wall time: 1 min 31 s Memory load: 8.5 GB 

2. Calculate equation of state) Wall time: 2.05 ms Memory load: Not relevant

3. Calculate mean first passage times) Wall Time: 17 min 56 s Memory load: 3 GB
